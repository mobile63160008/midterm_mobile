import 'package:midterm_mobile1/midterm_mobile1.dart' as midterm_mobile1;
import 'dart:io';

void main() {
  List<String> list = [];
  String? variable = stdin.readLineSync()!;
  var list1 = Cretoken(list, variable);
  var lastlist = cutTokenize(list1);
  print(lastlist);
}

//ข้อที่ 1 Exercise1
List<String> Cretoken(List<String> list, var info) {
  var sp = info.split(" ");
  for (int i = 0; i < sp.length; i++) {
    if (sp[i] != "") {
      list.add(sp[i]);
    } else {
      continue;
    }
  }
  return list;
}

List<String> cutTokenize(List<String> listData) {
  List<String> Symbol = ['(', ')', '+', '-', '*', '/'];
  List<String> NewList = [];
  String data = '';
  int i;
  for (i = 0; i < listData.length; i++) {
    if (Symbol.contains(listData[i])) {
      NewList.add(data);
      NewList.add(listData[i]);
      data = "";
    } else {
      data += listData[i];
    }
  }
  NewList.add(data);
  return NewList;
}

//ข้อที่ 2 Exercise2
List<String> Topostfix(List<String> listData) {
  var value_first = 0;
  var value_last = 5;
  List<String> Symbol = ['(', ')', '+', '-', '*', '/'];
  List<String> operator = [];
  List<String> postfix = [];
  for (int i = 0; i < listData.length; i++) {
    if (!Symbol.contains(listData)) {
      postfix.add(listData[i]);
    }
    if (Symbol.contains(listData)) {
      //value_first = postfix[i];
      if (operator.isNotEmpty) {
        // value_last = operator.last;
      } 
      while (operator.isNotEmpty &&
          operator.last != "(" &&
          value_first <= value_last) {
        postfix.add(operator.removeLast());
      }
      operator.add(postfix[i]);
    }
    if (postfix[i] == '(') {
      operator.add(postfix[i]);
    }
    if (postfix[i] == ')') {
      while (operator.last != "(") {
        postfix.add(operator.removeLast());
      }
      operator.remove("(");
    }
  }
  while (operator.isNotEmpty) {
    postfix.add(operator.removeLast());
  }
  return postfix;
}
